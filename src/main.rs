use std::collections::{BTreeSet, HashMap};

use Arch::*;

#[derive(Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
enum Arch {
    Unknown,

    AbberathTouched,
    ArakaaliTouched,
    ArcaneBuffer,
    Assassin,
    Berserker,
    Bloodletter,
    Bombardier,
    BoneBreaker,
    BrineKingTouched,
    Chaosweaver,
    Consecrator,
    CorpseDetonator,
    Corrupter,
    CrystalSkinned,
    Deadeye,
    DroughtBringer,
    Dynamo,
    Echoist,
    Effigy,
    EmpoweredElements,
    EmpoweringMinions,
    Entangler,
    Evocationist,
    Executioner,
    FlameStrider,
    Flameweaver,
    Frenzied,
    FrostStrider,
    Frostweaver,
    Gargantuan,
    Hasted,
    HeraldingMinions,
    Hexer,
    IcePrison,
    Incendiary,
    InnocenceTouched,
    Invulnerable,
    Juggernaut,
    KitavaTouched,
    LunarisTouched,
    MagmaBarrier,
    Malediction,
    ManaSiphoner,
    MirrorImage,
    Necromancer,
    Opulent,
    Overchanged,
    Permafrost,
    Rejuvinating,
    Sentinel,
    ShakariTouched,
    SolarisTouched,
    SoulConduit,
    SoulEater,
    SteelInfused,
    StormStrider,
    Stormweaver,
    TemporalBubble,
    Toxic,
    TreantHorde,
    Trickster,
    TukohamaTouched,
    Vamperic,
}

type Recipe = (Vec<Arch>, Vec<Arch>);

// impl Eq for Arch {}
// impl PartialOrd for Arch {
//     fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
//         Some(Ordering::Equal)
//     }
// }

fn main() {
    let mut arch_map = HashMap::new();
    arch_map.insert(AbberathTouched, (vec![FlameStrider, Frenzied, Rejuvinating], vec![KitavaTouched]));
    arch_map.insert(ArakaaliTouched, (vec![CorpseDetonator, Entangler, Assassin], vec![]));
    arch_map.insert(ArcaneBuffer, (vec![], vec![HeraldingMinions, TemporalBubble]));
    arch_map.insert(Assassin, (vec![Deadeye, Vamperic], vec![Trickster, ArakaaliTouched]));
    arch_map.insert(Berserker, (vec![], vec![Executioner, CrystalSkinned]));
    arch_map.insert(Bloodletter, (vec![], vec![Corrupter, Entangler]));
    arch_map.insert(Bombardier, (vec![], vec![Necromancer]));
    arch_map.insert(BoneBreaker, (vec![], vec![MagmaBarrier, TukohamaTouched]));
    arch_map.insert(BrineKingTouched, (vec![IcePrison, StormStrider, HeraldingMinions], vec![]));
    arch_map.insert(Chaosweaver, (vec![], vec![Hexer, Corrupter, EmpoweredElements]));
    arch_map.insert(Consecrator, (vec![], vec![TemporalBubble, Invulnerable]));
    arch_map.insert(CorpseDetonator, (vec![Necromancer, Incendiary], vec![KitavaTouched, ArakaaliTouched]));
    arch_map.insert(Corrupter, (vec![Bloodletter, Chaosweaver], vec![KitavaTouched, Effigy]));
    arch_map.insert(CrystalSkinned, (vec![Permafrost, Rejuvinating, Berserker], vec![]));
    arch_map.insert(Deadeye, (vec![], vec![Assassin, DroughtBringer]));
    arch_map.insert(DroughtBringer, (vec![Malediction, Deadeye], vec![ShakariTouched]));
    arch_map.insert(Dynamo, (vec![], vec![HeraldingMinions, ManaSiphoner]));
    arch_map.insert(Echoist, (vec![], vec![Trickster, Hexer, MirrorImage]));
    arch_map.insert(Effigy, (vec![Hexer, Malediction, Corrupter], vec![]));
    arch_map.insert(EmpoweredElements, (vec![Evocationist, SteelInfused, Chaosweaver], vec![]));
    arch_map.insert(EmpoweringMinions, (vec![Necromancer, Executioner, Gargantuan], vec![LunarisTouched, SolarisTouched]));
    arch_map.insert(Entangler, (vec![Toxic, Bloodletter], vec![ShakariTouched, ArakaaliTouched]));
    arch_map.insert(Evocationist, (vec![Flameweaver, Frostweaver, Stormweaver], vec![EmpoweredElements]));
    arch_map.insert(Executioner, (vec![Frenzied, Berserker], vec![TukohamaTouched, EmpoweringMinions]));
    arch_map.insert(FlameStrider, (vec![Flameweaver, Hasted], vec![AbberathTouched]));
    arch_map.insert(Flameweaver, (vec![], vec![FlameStrider, Evocationist]));
    arch_map.insert(Frenzied, (vec![], vec![Executioner, AbberathTouched]));
    arch_map.insert(FrostStrider, (vec![Frostweaver, Hasted], vec![LunarisTouched]));
    arch_map.insert(Frostweaver, (vec![], vec![FrostStrider, Evocationist]));
    arch_map.insert(Gargantuan, (vec![], vec![Rejuvinating, SoulEater, EmpoweringMinions]));
    arch_map.insert(Hasted, (vec![], vec![FlameStrider, FrostStrider, StormStrider]));
    arch_map.insert(HeraldingMinions, (vec![Dynamo, ArcaneBuffer], vec![BrineKingTouched]));
    arch_map.insert(Hexer, (vec![Chaosweaver, Echoist], vec![TemporalBubble, Effigy]));
    arch_map.insert(IcePrison, (vec![Permafrost, Sentinel], vec![BrineKingTouched]));
    arch_map.insert(Incendiary, (vec![], vec![CorpseDetonator, MagmaBarrier]));
    arch_map.insert(InnocenceTouched, (vec![SolarisTouched, LunarisTouched, ManaSiphoner, MirrorImage], vec![]));
    arch_map.insert(Invulnerable, (vec![Sentinel, Juggernaut, Consecrator], vec![LunarisTouched, SolarisTouched]));
    arch_map.insert(Juggernaut, (vec![], vec![TemporalBubble, Invulnerable]));
    arch_map.insert(KitavaTouched, (vec![TukohamaTouched, AbberathTouched, Corrupter, CorpseDetonator], vec![]));
    arch_map.insert(LunarisTouched, (vec![Invulnerable, FrostStrider, EmpoweringMinions], vec![InnocenceTouched]));
    arch_map.insert(MagmaBarrier, (vec![Incendiary, BoneBreaker], vec![TukohamaTouched, SolarisTouched]));
    arch_map.insert(Malediction, (vec![], vec![DroughtBringer, Effigy]));
    arch_map.insert(ManaSiphoner, (vec![Consecrator, Dynamo], vec![InnocenceTouched]));
    arch_map.insert(MirrorImage, (vec![Echoist, SoulConduit], vec![InnocenceTouched]));
    arch_map.insert(Necromancer, (vec![Bombardier, Overchanged], vec![SoulEater, CorpseDetonator, EmpoweringMinions]));
    arch_map.insert(Opulent, (vec![], vec![]));
    arch_map.insert(Overchanged, (vec![], vec![Trickster, Necromancer]));
    arch_map.insert(Permafrost, (vec![], vec![IcePrison, CrystalSkinned]));
    arch_map.insert(Rejuvinating, (vec![Gargantuan, Vamperic], vec![CrystalSkinned, AbberathTouched]));
    arch_map.insert(Sentinel, (vec![], vec![TreantHorde, IcePrison, Invulnerable]));
    arch_map.insert(ShakariTouched, (vec![Entangler, SoulEater, DroughtBringer], vec![]));
    arch_map.insert(SolarisTouched, (vec![Invulnerable, MagmaBarrier, EmpoweringMinions], vec![InnocenceTouched]));
    arch_map.insert(SoulConduit, (vec![], vec![SoulEater, MirrorImage]));
    arch_map.insert(SoulEater, (vec![SoulConduit, Necromancer, Gargantuan], vec![ShakariTouched]));
    arch_map.insert(SteelInfused, (vec![], vec![TreantHorde, EmpoweredElements]));
    arch_map.insert(StormStrider, (vec![Stormweaver, Hasted], vec![BrineKingTouched]));
    arch_map.insert(Stormweaver, (vec![], vec![StormStrider, Evocationist]));
    arch_map.insert(TemporalBubble, (vec![Juggernaut, Hexer, ArcaneBuffer], vec![]));
    arch_map.insert(Toxic, (vec![], vec![TreantHorde, Entangler]));
    arch_map.insert(TreantHorde, (vec![Toxic, Sentinel, SteelInfused], vec![]));
    arch_map.insert(Trickster, (vec![Overchanged, Assassin, Echoist], vec![]));
    arch_map.insert(TukohamaTouched, (vec![BoneBreaker, Executioner, MagmaBarrier], vec![KitavaTouched]));
    arch_map.insert(Vamperic, (vec![], vec![Assassin, Rejuvinating]));

    let mut all_arch = BTreeSet::new();
    for recipe in arch_map.iter() {
        all_arch.insert(recipe.0);
        (&(recipe.1).0).iter().for_each(|dep| { all_arch.insert(dep); });
        (&(recipe.1).1).iter().for_each(|dep| { all_arch.insert(dep); });
    }
    // println!("All Arch: {:?}\n", all_arch);

    let base_types: BTreeSet<&Arch> = arch_map.iter()
        .filter(|arch| (*arch.1).0.len() == 0)
        .map(|arch| arch.0)
        .collect();
    println!("Base Types: {:?} #{}\n", base_types, base_types.len());

    // println!("Includes: {:?}\n", base_types.contains(&KitavaTouched));

    let incomplete: BTreeSet<&Arch> = arch_map.iter()
        .filter(|entry| entry.1.1.contains(&Unknown))
        .map(|recipe| recipe.0)
        .collect();
    println!("Incomplete recipes: {:?}\n", incomplete);

    let mut needed = BTreeSet::new();
    for arch in incomplete {
        app_deps(&arch_map, &mut needed, arch);
    }
    println!("Needed: {:?}\n", needed);

    let not_needed: BTreeSet<&Arch> = all_arch
        .difference(&needed).cloned().collect::<BTreeSet<&Arch>>();
        // .intersection(&base_types).cloned().collect();
    println!("Not Needed: {:?}\n", not_needed);
}

fn app_deps<'a>(arch_map: &'a HashMap<Arch, Recipe>, needed: &mut BTreeSet<&'a Arch>, arch: &'a Arch) {
    let recipe = arch_map.get(arch)
        .expect(&format!("Could not find entry for {:?}", arch));

    needed.insert(arch);

    for dep in recipe.0.iter() {
        app_deps(arch_map, needed, dep);
    }
}
